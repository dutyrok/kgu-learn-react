import React from 'react';
// Импортируем компонент Welcome
import { Welcome } from './components/Welcome/Welcome';
import { Button } from './components/Button/Button';
import Message from './components/Message/Message';
import ColoredText from './components/ColoredText/ColoredText';


function App() {
  return (
    <div>
      {/* Используем компонент Welcome и передаем ему prop 'name' */}
      <Welcome name="Alice" />
      <Button />
      <Message title="Hello" text="Its first message"/>
      <Message title="Important" text="Say goodbye"/>
      <ColoredText color="green" text="This is green text"/>
      <ColoredText color="red" text="This is red text"/>
    </div>
  );
}

export default App;
