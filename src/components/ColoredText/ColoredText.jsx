import React from "react";

export default function ColoredText(props){
    return(
        <p style={{color: props.color}}>{props.text}</p>
    )
}
