import React from "react";

export default function Message(props){
    return(
        <div>
            <hr size="2"></hr>
            <h2>{props.title}</h2>
            <p>{props.text}</p>
            <hr size="2"></hr>
        </div>
    )
}